//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, StatusBar, Image } from 'react-native';
import QRCode from 'react-native-qrcode-svg';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Container, Header, Left, Body, Title } from 'native-base';

const arrow = '../../assets/arrow.png';
// create a component
class Uposbarcode extends Component {
    state = {
        text: 'Bang Mifta Punya Jin Banyak',
    };

    render() {

        return (
            <Container>
                <StatusBar backgroundColor="#1018F2" barStyle="light-content" />
                <Header style={{ backgroundColor: '#1018F2' }} androidStatusBarColor='#1018F2'>
                    <Left>
                        <View style={{
                            marginLeft: hp("1%")
                        }}>
                            <Image source={require(arrow)} />
                        </View>

                    </Left>
                    <Body>
                        <Title style={{ fontSize: 24, color: 'white' }}>Upos QR Code</Title>
                    </Body>
                </Header>
                <View style={{
                    marginTop: hp("10%"),
                    alignItems: "center"
                }}>
                    <QRCode
                        value={this.state.text}
                        size={200}
                        logoBackgroundColor='transparent'
                    />
                    <Text style={{
                        marginTop: hp("5%"),
                        alignItems: "center"
                    }}>
                        Please Scan To Pay
                    </Text>
                </View>
            </Container>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
});

//make this component available to the app
export default Uposbarcode;
