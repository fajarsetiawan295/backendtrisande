//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, StatusBar } from 'react-native';
import { Calculator, CalculatorInput } from 'react-native-calculator';
import Barcode from 'react-native-barcode-builder';
// import QRCode from 'react-native-qrcode-svg';


// create a component
class Kalkulator extends Component {
    render() {
        return (
            <View style={{ flex: 1 }}>
                <StatusBar backgroundColor="#1018F2" barStyle="light-content" />
                <Calculator style={{ flex: 1 }} />
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
});

//make this component available to the app
export default Kalkulator;
