//import liraries
import React, { Component } from 'react';
import { TouchableOpacity, Text, View, StyleSheet, Image, Dimensions, ImageBackground, ScrollView, SafeAreaView, CheckBox, StatusBar } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import AnimateLoadingButton from 'react-native-animate-loading-button';
import { TextInput } from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome';
import { theme } from '../../components/theme';
import { Container } from 'native-base';


const BackgroundBlue = '../../assets/Rectangle.png';
const Logo = '../../assets/logokoperasi.png';

// create a component
export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            editable: true,
            emailInput: '',
            passwordInput: '',
            checked: false,
        }
    }

    _onPressHandler() {
        this.loadingButton.showLoading(true);

        // mock
        setTimeout(() => {
            this.loadingButton.showLoading(false);
            this.props.navigation.navigate("Home");
        }, 2000);
    }

    render() {
        const { checked } = this.state;
        return (
            <ScrollView showsVerticalScrollIndicator={false}>
                <SafeAreaView style={styles.container}>
                    <View>
                        <StatusBar  backgroundColor="#1464cc" barStyle="light-content" />
                        <Image source={require(BackgroundBlue)} style={styles.backgroundBlue} />
                        <View style={styles.textHeadStyle}>
                            <View style={{ marginRight: wp('50%') }}>
                                <Text style={styles.textHello}>Welcome,</Text>
                            </View>
                            <View>
                                <Text style={styles.textWelcome}>Koperasi Digital{'\n'}Indonesia</Text>
                            </View>
                            <View style={{ marginLeft: wp('60%') }}>
                                <Text style={styles.textHello}>Login</Text>
                            </View>

                        </View>
                    </View>

                    <View style={styles.Form}>
                        <TextInput
                            style={styles.inputText}
                            mode='outlined'
                            label='Handphone'
                            placeholder="Enter Number"
                            borderRadius={16}
                            keyboardType='numeric'
                            maxLength={13}
                            value={this.state.emailInput}
                            onChangeText={(email) => this.setState({ emailInput: email })}
                            theme={theme}
                        />

                    </View>
                    <View style={styles.Form}>
                        <TextInput
                            style={styles.inputText}
                            mode='outlined'
                            label='Password'
                            placeholder="Enter Password"
                            disable={true}
                            value={this.state.passwordInput}
                            secureTextEntry={true}
                            onChangeText={(password) => this.setState({ passwordInput: password })}
                            theme={theme}
                        />
                    </View>
                    <View style={styles.viewCheckbox}>
                        <View style={styles.rowCheckbox}>
                            <CheckBox
                                checked={this.state.checked}
                            />
                            <Text style={styles.rowText}>Remember Me</Text>
                        </View>
                        <TouchableOpacity>
                            <View style={styles.viewText}>
                                <Text style={styles.rowText}>Forgot Password?</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.Button}>
                        <AnimateLoadingButton
                            ref={c => (this.loadingButton = c)}
                            width={200}
                            height={50}
                            title="LOGIN"
                            titleFontSize={16}
                            titleColor="rgb(255,255,255)"
                            backgroundColor="#1464cc"
                            borderRadius={27}
                            shadowRadius={8}
                            shadowColor="rgba(0, 0, 0, 0.1)"
                            onPress={this._onPressHandler.bind(this)}
                        />
                        <Text style={styles.TextBott}>{'\n'}New User ?{' '}
                            <Text
                                style={styles.textRegis}
                                onPress={() => this.props.navigation.navigate('Register')}>
                                Register !
                            </Text>
                        </Text>
                    </View>
                    <View style={styles.center}>
                        <Image source={require(Logo)} style={styles.Logo} />
                    </View>
                    <Text style={{ marginBottom: hp('3%') }}>
                        Powered By <Text style={{ color: '#003cff' }}>MCP</Text>
                    </Text>
                </SafeAreaView>
            </ScrollView>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: '#fff',
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    viewCheckbox: {
        width: wp('85%'),
        flexDirection: 'row'
    },
    rowCheckbox: {
        flexDirection: 'row',
        marginTop: 10,
    },
    rowText: {
        marginTop: 5,
        // fontFamily: 'DINPro'
    },
    viewText: {
        marginLeft: wp('15%'),
        marginTop: 10
    },
    textHeadStyle: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: wp('30%'),
        height: hp('30%'),
        width: wp('100%'),
        alignItems: 'center',
        justifyContent: 'center',
        // fontFamily: 'DINPro',
        marginTop: hp('10%'),


    },
    textHello: {
        color: 'white',
        fontSize: 25,
        // fontFamily: 'DINPro'
    },
    textWelcome: {
        textAlign: 'center',
        color: 'white',
        fontSize: 35,
        // fontFamily: 'DINPro'
    },
    Button: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
        marginBottom: 30,
        // fontFamily: 'DINPro'
    },
    TextBott: {
        color: 'black',
        letterSpacing: -0.32,
        fontSize: 16,
        textAlign: 'center',
        // fontFamily: 'DINPro'
    },
    TextForm: {
        color: 'white',
        fontSize: 16,
        // fontFamily: 'DINPro'
    },
    Form: {
        shadowColor: 'rgba(0, 0, 0, 0.1)',
        shadowOffset: { width: 3, height: 0 },
        shadowRadius: 6,
        borderRadius: 14,
        marginTop: 10,
        width: wp('85%')
    },
    backgroundBlue: {
        width: wp('105%'),
        height: hp('50%'),
        alignItems: 'center',
        justifyContent: 'center',
        shadowColor: 'rgba(0, 0, 0, 0.12)',
        shadowOffset: { width: 10, height: 0 },
        resizeMode: 'cover',
        // marginRight: wp('0%'),
        marginTop: -5,
        marginBottom: hp('5%')

    },
    Logo: {
        shadowColor: 'rgba(0, 0, 0, 0.16)',
        shadowOffset: { width: 1, height: 0 },
        marginTop: hp('2%'),
        marginBottom: hp('3%'),
        resizeMode: 'stretch',
        width: wp('60%'),
        height: wp('60%')
    },
    TextHead: {
        color: 'white',
        // fontFamily: 'DinPro',
        fontSize: 35,
        textAlign: 'center',
        fontWeight: '500'
    },
    inputText: {
        height: hp('8%')
    },
    textRegis: {
        color: '#ffc100',
        fontWeight: '500',
        // fontFamily: 'DINPro'
    },
    searchIcon: {
        padding: 10,
    }
});

        //make this component available to the app

