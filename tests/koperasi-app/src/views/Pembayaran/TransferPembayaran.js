//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, StatusBar, TouchableOpacity, Image, ScrollView } from 'react-native';
import { Container, Header, Left, Body, Title, Row, Accordion, Content } from 'native-base';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/AntDesign';
import IconFontawesome from 'react-native-vector-icons/FontAwesome';

const dataArray = [
    { title: "Via ATM BCA", content: "1.  Masukkan kartu atm dan pin bca anda \n2. Pilih menu TRANSAKSI LAINNYA > TRANFER > KE REKENING BCA VIRTUAL ACCOUNT \n3. Masukkan 3901 123 123 123 sebagai rekening tujuan \n4. Masukkan jumlah Top Up yang dibayarkan \n5. Ikuti instruksi selanjutnya \n6. Transaksi minimal Rp 10.000" },
    { title: "Via Klik BCA", content: "1.  Masukkan kartu atm dan pin bca anda \n2. Pilih menu TRANSAKSI LAINNYA > TRANFER > KE REKENING BCA VIRTUAL ACCOUNT \n3. Masukkan 3901 123 123 123 sebagai rekening tujuan \n4. Masukkan jumlah Top Up yang dibayarkan \n5. Ikuti instruksi selanjutnya \n6. Transaksi minimal Rp 10.000" },
    { title: "M-BCA Mobile", content: "1.  Masukkan kartu atm dan pin bca anda \n2. Pilih menu TRANSAKSI LAINNYA > TRANFER > KE REKENING BCA VIRTUAL ACCOUNT \n3. Masukkan 3901 123 123 123 sebagai rekening tujuan \n4. Masukkan jumlah Top Up yang dibayarkan \n5. Ikuti instruksi selanjutnya \n6. Transaksi minimal Rp 10.000" }
];

// create a component
class TransferPembayaran extends Component {
    render() {
        return (
            <Container>

                <StatusBar backgroundColor="#1018F2" barStyle="light-content" />
                {/* head */}
                <Header style={{ backgroundColor: '#1018F2' }} androidStatusBarColor='#1018F2'>
                    <TouchableOpacity>
                        <Left style={{
                            flexDirection: "row"
                        }}>
                            <Icon style={{ marginTop: wp("1%"), color: 'white' }} name='leftcircleo' size={24} color='white' />
                            <Title style={{ marginLeft: wp("2%"), fontSize: 20, color: 'white', marginTop: wp('1%') }}>BCA</Title>
                        </Left>
                    </TouchableOpacity>
                    <Body>
                    </Body>
                </Header>
                <View style={{ width: wp('100%'), height: hp('20%') }}>
                    <View style={{ marginLeft: wp('5%'), marginTop: wp('5%') }}>
                        <Text style={{}}>Konfirmasi pembayaran anda</Text>
                    </View>
                    <View style={{ marginTop: wp('3%'), backgroundColor: '#f2f2f2', width: wp('100%'), height: 2 }} />
                    <TouchableOpacity style={{ flexDirection: 'row', margin: wp('3%') }}>
                        <View>
                            <Image style={{ width: wp('13%'), height: hp('5%') }} source={require('../../assets/BCA.png')} />
                        </View>
                        <View style={{ marginTop: wp('2%'), marginLeft: wp('2%') }}>
                            <Text>Transfer Bank BCA</Text>
                        </View>
                        <View style={{ flexDirection: 'row', marginTop: wp('2%'), marginLeft: wp('25%') }}>
                            <Text>Ganti</Text>
                            <IconFontawesome name={'chevron-right'} style={{ marginTop: 2, marginLeft: 5 }} />
                        </View>
                    </TouchableOpacity>
                    <View style={{ marginTop: wp('3%'), backgroundColor: '#f2f2f2', width: wp('100%'), height: 2 }} />
                </View>
                <View style={{ marginTop: wp('3%'), backgroundColor: '#f2f2f2', width: wp('100%'), height: hp('6%') }} />
                <View style={{ padding: 12 }}>
                    <View>
                        <Text>Biaya detail</Text>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text style={{ color: '#0A0E8F', fontSize: wp('6%') }}>Total</Text>
                        <Text style={{ color: '#0A0E8F', fontSize: wp('6%') }}>Rp 5950</Text>
                    </View>
                    <View>
                        <Text>Nomer Akun : 81234567890</Text>
                    </View>
                </View>
                <View style={{ marginTop: wp('3%'), backgroundColor: '#f2f2f2', width: wp('100%'), height: 2 }} />
                <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: wp('5%') }}>
                    <Text style={{ width: wp('70%'), justifyContent: 'center', textAlign: 'center' }}>Saldo akan masuk secara otomatis setelah pembayaran transaksi</Text>
                </View>
                <View style={{ marginTop: wp('3%'), backgroundColor: '#f2f2f2', flex:1, alignItems:'center' }}>
                    <Image source={require('../../assets/demoonline.png')} style={{marginTop:wp('10%')}}/>
                    <TouchableOpacity style={{width:wp('80%'), height:hp('5%'), backgroundColor:'#1018F2', marginTop:wp('10%'), justifyContent:'center', alignItems:'center'}}>
                        <Text style={{color:'white', fontWeight:'600'}}>BAYAR Rp 5950</Text>
                    </TouchableOpacity>
                </View>
            </Container>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
});

//make this component available to the app
export default TransferPembayaran;
