//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, StatusBar, Image, ScrollView, KeyboardAvoidingView, Button, TouchableOpacity, show } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { TextInput } from 'react-native-paper';
import { Container, Header, Left, Body, Title, Row } from 'native-base';
import Icon from 'react-native-vector-icons/AntDesign';
// create a component

const debit = '../../assets/debit.png';
const visa = '../../assets/visa.png';
const arrowback = '../../assets/arrowback.png';
const store = '../../assets/store.png';
class MetodePembayaran extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Merchant: [
                {
                    id: "1",
                    Bank: "Alfamart",
                },
                {
                    id: "1",
                    Bank: "indomart",
                },
            ],
            bank: [
                {
                    id: "1",
                    Bank: "BNI",
                    gambar: require("../../assets/BNI.png")
                },
                {
                    id: "1",
                    Bank: "BCA",
                    gambar: require("../../assets/BCA.png")
                },
                {
                    id: "1",
                    Bank: "BRI",
                    gambar: require("../../assets/BRI.png")
                },
                {
                    id: "1",
                    Bank: "Mandiri",
                    gambar: require("../../assets/mandiri.png")
                },
            ],
        }
    }
    render() {
        return (
            <Container>
                <StatusBar backgroundColor="#1018F2" barStyle="light-content" />
                {/* head */}
                <Header style={{ backgroundColor: '#1018F2' }} androidStatusBarColor='#1018F2'>
                    <TouchableOpacity>
                        <Left style={{
                            flexDirection: "row"
                        }}>
                            <Icon style={{ marginTop: wp("1%"), color: 'white' }} name='leftcircleo' size={24} color='white' />
                            <Title style={{ marginLeft: wp("2%"), fontSize: 20, color: 'white', marginTop: wp('1%') }}>Metode Pembayaran</Title>
                        </Left>
                    </TouchableOpacity>
                    <Body>
                    </Body>
                </Header>
                {/* and head */}
                {/* saldo */}
                <ScrollView>
                    <View>
                        <View style={{
                            padding: wp("5%")
                        }}>
                            <Text style={{
                                fontSize: wp("4%")
                            }}>PILIH VIRTUAL ACCOUNT</Text>
                        </View>
                        <View style={{ backgroundColor: 'black', width: wp("100"), height: hp("0.1%") }}></View>
                        <View style={{
                            alignItems: "center",
                            justifyContent: "center",
                            padding: wp("5%")
                        }}>
                            <Text>Saldo Saat Ini</Text>
                            <Text style={{ color: "blue", padding: wp("2%") }}>Rp. 0</Text>
                        </View>
                        <View style={{ backgroundColor: 'black', width: wp("100"), height: hp("0.1%") }}></View>
                        {/* end saldo */}
                        {/* metode pembayaran visa */}
                        <View style={{
                            backgroundColor: "#F5F5F5",
                        }}>
                            <Text style={{
                                padding: wp("5%")
                            }}>PILIH METODE ISI SALDO</Text>

                            <View style={{
                                marginTop: wp("1%"),
                                backgroundColor: "white",
                                flexDirection: "row",
                                height: wp("15%")
                            }}>
                                <Image source={require(debit)} style={styles.card} />
                                <Text style={{
                                    marginTop: hp("2%"),
                                    marginLeft: hp("3%")
                                }}>Kartu debit</Text>
                                <Image source={require(visa)} style={styles.visa} />
                                <Icon style={{
                                    marginTop: hp("3%"),
                                    marginLeft: hp("4%")
                                }} name='right' size={20} color='black' />
                            </View>

                            {/* end visa */}

                            {/* atm */}
                            <View style={{
                                marginTop: wp("3%"),
                                backgroundColor: "white",
                            }}>
                                <View style={{
                                    flexDirection: "row",
                                }}>
                                    <Image source={require(arrowback)} style={styles.card} />
                                    <Text style={{
                                        marginTop: hp("2%"),
                                        marginLeft: hp("3%")
                                    }}>Transfer bank</Text>
                                </View>
                                {
                                    this.state.bank.map((item, index) => (
                                        <TouchableOpacity>
                                            <View style={{
                                                flexDirection: "row",
                                                justifyContent: "space-between",
                                            }}>
                                                <Text style={{
                                                    marginTop: hp("2%"),
                                                    marginLeft: hp("9%")
                                                }}>{item.Bank}</Text>
                                                <View style={{
                                                    marginTop: hp("3%"),
                                                    flexDirection: "row"

                                                }}>
                                                    <Image source={item.gambar} />
                                                    <Icon style={{
                                                        marginRight: hp("2%")
                                                    }} name='right' size={20} color='black' />

                                                </View>

                                            </View>
                                        </TouchableOpacity>


                                    ))
                                }
                            </View>
                            {/* end atm */}
                            <View style={{
                                marginTop: wp("3%"),
                                backgroundColor: "white",
                            }}>
                                <View style={{
                                    flexDirection: "row",
                                }}>
                                    <Image source={require(store)} style={styles.card} />
                                    <Text style={{
                                        marginTop: hp("2%"),
                                        marginLeft: hp("3%")
                                    }}>Via Merchant</Text>
                                </View>
                                {
                                    this.state.Merchant.map((item, index) => (
                                        <TouchableOpacity>
                                            <View style={{
                                                flexDirection: "row",
                                                justifyContent: "space-between",
                                            }}>
                                                <Text style={{
                                                    marginTop: hp("3%"),
                                                    marginLeft: hp("9%")
                                                }}>{item.Bank}</Text>
                                                <View style={{
                                                    marginTop: hp("3%"),
                                                    flexDirection: "row"

                                                }}>
                                                    <Icon style={{
                                                        marginRight: hp("2%")
                                                    }} name='right' size={20} color='black' />

                                                </View>

                                            </View>
                                        </TouchableOpacity>

                                    ))
                                }
                            </View>
                            <View style={{height:100}}/>
                        </View>
                    </View>
                </ScrollView>
            </Container>
        );
    }
}
// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
    card: {
        shadowColor: 'rgba(0, 0, 0, 0.16)',
        shadowOffset: { width: 1, height: 0 },
        marginTop: hp('2%'),
        resizeMode: 'stretch',
        marginLeft: wp('2%'),
        width: wp('8%'),
        height: hp('3%')

    },
    visa: {
        shadowColor: 'rgba(0, 0, 0, 0.16)',
        shadowOffset: { width: 1, height: 0 },
        marginTop: hp('3%'),
        resizeMode: 'stretch',
        marginLeft: wp('20%'),
        width: wp('25%'),
        height: hp('3%')
    },
    bank: {
        shadowColor: 'rgba(0, 0, 0, 0.16)',
        shadowOffset: { width: 1, height: 0 },
        marginTop: hp('2%'),
        resizeMode: 'stretch',
        flexDirection: "row-reverse",
        width: wp('25%'),
        height: hp('3%')

    },
});

//make this component available to the app
export default MetodePembayaran;
