//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, StatusBar, TouchableOpacity, Image, ScrollView } from 'react-native';
import { Container, Header, Left, Body, Title, Row, Accordion, Content } from 'native-base';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/AntDesign';

const dataArray = [
    { title: "Via ATM BCA", content: "1.  Masukkan kartu atm dan pin bca anda \n2. Pilih menu TRANSAKSI LAINNYA > TRANFER > KE REKENING BCA VIRTUAL ACCOUNT \n3. Masukkan 3901 123 123 123 sebagai rekening tujuan \n4. Masukkan jumlah Top Up yang dibayarkan \n5. Ikuti instruksi selanjutnya \n6. Transaksi minimal Rp 10.000" },
    { title: "Via Klik BCA", content: "1.  Masukkan kartu atm dan pin bca anda \n2. Pilih menu TRANSAKSI LAINNYA > TRANFER > KE REKENING BCA VIRTUAL ACCOUNT \n3. Masukkan 3901 123 123 123 sebagai rekening tujuan \n4. Masukkan jumlah Top Up yang dibayarkan \n5. Ikuti instruksi selanjutnya \n6. Transaksi minimal Rp 10.000" },
    { title: "M-BCA Mobile", content: "1.  Masukkan kartu atm dan pin bca anda \n2. Pilih menu TRANSAKSI LAINNYA > TRANFER > KE REKENING BCA VIRTUAL ACCOUNT \n3. Masukkan 3901 123 123 123 sebagai rekening tujuan \n4. Masukkan jumlah Top Up yang dibayarkan \n5. Ikuti instruksi selanjutnya \n6. Transaksi minimal Rp 10.000" }
];

// create a component
class MenungguPembayaran extends Component {
    render() {
        return (
            <Container>

                <StatusBar backgroundColor="#1018F2" barStyle="light-content" />
                {/* head */}
                <Header style={{ backgroundColor: '#1018F2', height: hp('20%') }} androidStatusBarColor='#1018F2'>
                    <TouchableOpacity>
                        <Left style={{
                            flexDirection: "row"
                        }}>
                            <Icon style={{ marginTop: wp("1%"), color: 'white' }} name='leftcircleo' size={24} color='white' />
                            <Title style={{ marginLeft: wp("2%"), fontSize: 20, color: 'white', marginTop: wp('1%') }}>Menunggu Pembayaran</Title>
                        </Left>
                    </TouchableOpacity>
                    <Body>
                    </Body>
                </Header>
                <View style={{
                        backgroundColor: 'white', width: wp('90%'), height: hp('20%'), position: 'absolute', top: wp('20%'), justifyContent: 'center', alignItems: 'center', borderRadius: wp('2%'), left: wp('5%'), shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 3,
                        },
                        shadowOpacity: 0.27,
                        shadowRadius: 4.65,
                        elevation: 6
                    }}>
                        <Text style={{ fontSize: wp('5%') }}>Sisa Waktu</Text>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-around', marginTop: wp('2%') }}>
                            <Text style={{ fontSize: wp('4%'), fontWeight: '800' }}>24 Jam</Text>
                            <Text style={{ marginLeft: wp('5%'), fontSize: wp('4%'), fontWeight: '800' }}>0 Menit</Text>
                            <Text style={{ marginLeft: wp('5%'), fontSize: wp('4%'), fontWeight: '800' }}>0 Detik</Text>
                        </View>
                    </View>
                <ScrollView style={{flex:1, marginTop:wp('30%')}}>
                    <View style={{
                        backgroundColor: 'white', width: wp('100%'), height: hp('30%'), shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 3,
                        },
                        shadowOpacity: 0.27,
                        shadowRadius: 4.65,
                        elevation: 6
                    }}>
                        <Text style={{ fontWeight: '300', color: '', padding: 12 }}>Nomor Akun Virtual BCA</Text>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={{ color: '#0A0E8F', fontSize: wp('7%'), marginLeft: 12 }}>3901 1234 1234 123</Text>
                            <TouchableOpacity style={{ borderColor: '#0A0E8F', borderWidth: 2, marginLeft: wp('5%'), width: wp('20%'), justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ color: '#0A0E8F', fontSize: wp('3%') }}>Salin Kode</Text>
                            </TouchableOpacity>
                        </View>
                        <Text style={{ marginLeft: 12, marginTop: wp('3%') }}>pulsa Telkomsel 5,000 - 081519357339</Text>
                        <View style={{ marginTop: wp('3%'), backgroundColor: '#c7c7c7', width: wp('100%'), height: 2 }} />
                        <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: wp('5%') }}>
                            <Text style={{ width: wp('70%'), justifyContent: 'center', textAlign: 'center' }}>Saldo akan masuk secara otomatis setelah pembayaran transaksi</Text>
                        </View>
                    </View>
                    {/* <View style={{ backgroundColor: '#ebebeb', width: wp('100%'), height: hp('35%'), alignItems: 'center' }}>
                    <Image style={{ marginTop: wp('5%') }} source={require('../../assets/demoonline.png')} />
                    <TouchableOpacity style={{ backgroundColor: '#1018F2', width:wp('60%'), height:hp('7%'), marginTop:wp('5%'), justifyContent:'center', alignItems:'center' }}>
                        <Text style={{color:'white', fontWeight:'600'}}>BAYAR Rp. 5950</Text>
                    </TouchableOpacity>
                </View> */}
                    <Content padder>
                        <Accordion headerStyle={'white'} dataArray={dataArray} expanded={0} />
                    </Content>
                </ScrollView>
            </Container>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
});

//make this component available to the app
export default MenungguPembayaran;
