//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, ScrollView } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/FontAwesome';
import Slideshow from 'react-native-slideshow';

// create a component
class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            position: 1,
            interval: null,
            dataSource: [
                {
                    title: 'Title 1',
                    caption: 'Caption 1',
                    url: 'http://placeimg.com/640/480/any',
                }, {
                    title: 'Title 2',
                    caption: 'Caption 2',
                    url: 'http://placeimg.com/640/480/any',
                }, {
                    title: 'Title 3',
                    caption: 'Caption 3',
                    url: 'http://placeimg.com/640/480/any',
                },
            ],
        };
    }
    componentWillMount() {
        this.setState({
            interval: setInterval(() => {
                this.setState({
                    position: this.state.position === this.state.dataSource.length ? 0 : this.state.position + 1
                });
            }, 2000)
        });
    }

    componentWillUnmount() {
        clearInterval(this.state.interval);
    }
    render() {
        return (
            
            <View style={styles.container}>
                <ScrollView contentContainerStyle={{ flexGrow: 1, height: hp('150%') }}>
                    <View style={{ width: wp('100%'), backgroundColor: '#1464CC', height: hp('50%') }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <TouchableOpacity>
                                <View style={{ marginLeft: wp('5%'), marginTop: wp('5%') }}><Icon name="user" color="white" size={30} /></View>
                            </TouchableOpacity>
                            <TouchableOpacity>
                                <View style={{ marginRight: wp('5%'), marginTop: wp('5%') }}><Icon name="cog" color="white" size={30} /></View>
                            </TouchableOpacity>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={{ width: wp('20%'), height: wp('20%'), borderRadius: wp('20%'), backgroundColor: 'white', marginLeft: wp('5%'), marginTop: wp('5%') }}>

                            </View>
                            <View style={{ width: wp('20%'), height: wp('20%'), borderRadius: wp('20%'), marginTop: wp('5%'), marginRight: wp('20%') }}>
                                <Text style={{ color: 'white', width: wp('40%'), fontWeight: '600', fontSize: wp('4%') }}>Arief Advani</Text>
                                <View style={{ width: wp('40%'), height: hp('5%'), backgroundColor: 'white', justifyContent: 'center', alignItems: 'center', borderRadius: wp('1%') }}>
                                    <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                                        <View>
                                            <Text style={{ color: '#919191' }}>Saldo</Text>
                                        </View>
                                        <View>
                                            <Text style={{ marginLeft: wp('2%'), color: '#F4BE0C' }}>Rp 500.000</Text>
                                        </View>
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row', marginTop: wp('1%') }}>
                                    <Text style={{ color: 'white', fontWeight: '600', fontSize: wp('4%') }}>Poin</Text>
                                    <Text style={{ color: 'white', fontWeight: '600', fontSize: wp('4%'), marginLeft: wp('2%') }}>0</Text>
                                </View>
                            </View>
                            <View style={{ width: wp('20%'), height: wp('20%'), borderRadius: wp('20%'), marginRight: wp('5%'), marginTop: wp('10%') }}>
                                <TouchableOpacity style={{ width: wp('20%'), height: hp('5%'), backgroundColor: 'white', justifyContent: 'center', alignItems: 'center', borderRadius: wp('1%') }}>
                                    <Text>Isi Saldo</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                            <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center', marginLeft: wp('3%') }}>
                                <Image source={require('../assets/register.png')} />
                                <Text style={{ color: 'white', fontWeight: '600' }}>Register</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <Image source={require('../assets/facerecog.png')} />
                                <Text style={{ color: 'white', fontWeight: '600' }}>FR</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <Image source={require('../assets/scan.png')} />
                                <Text style={{ color: 'white', fontWeight: '600' }}>Scan</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }}>
                                <Image source={require('../assets/transfer.png')} />
                                <Text style={{ color: 'white', fontWeight: '600' }}>Transfer</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center', marginRight: wp('3%') }}>
                                <Image source={require('../assets/eduit.png')} />
                                <Text style={{ color: 'white', fontWeight: '600' }}>E-Duit</Text>
                            </TouchableOpacity>
                        </View>
                        
                        <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: wp('5%') }}>
                            <View style={{
                                backgroundColor: 'white', width: wp('90%'), height: hp('50%'), borderRadius: wp('5%'), shadowColor: "#000",
                                shadowOffset: {
                                    width: 0,
                                    height: 2,
                                },
                                shadowOpacity: 0.23,
                                shadowRadius: 2.62,
                                elevation: 4,
                                justifyContent: 'space-around'
                            }}>
                                
                                <View style={{ marginLeft: wp('3%'), marginTop: wp('2%') }}>
                                    <Text style={{ fontSize: wp('5%') }}>Layanan</Text>
                                    <Text>Lakukan pembayaran dengan cepat</Text>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                                    <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }}>
                                        <Image source={require('../assets/upos.png')} style={{ width: wp('13%'), height: wp('13%') }} />
                                        <Text>Upos</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }}>
                                        <Image source={require('../assets/pasardigital.png')} />
                                        <Text>Pasar</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }}>
                                        <Image source={require('../assets/travel.png')} />
                                        <Text>Travel</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }}>
                                        <Image source={require('../assets/topup.png')} />
                                        <Text>Top Up</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                                    <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }}>
                                        <Image source={require('../assets/pulsadata.png')} />
                                        <Text>Pulsa Data</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }}>
                                        <Image source={require('../assets/pdam.png')} />
                                        <Text>PDAM</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }}>
                                        <Image source={require('../assets/games.png')} />
                                        <Text>Games</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }}>
                                        <Image source={require('../assets/pln.png')} />
                                        <Text>PLN</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                                    <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }}>
                                        <Image source={require('../assets/internet.png')} />
                                        <Text>Internet</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }}>
                                        <Image source={require('../assets/bpjs.png')} />
                                        <Text>BPJS</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }}>
                                        <Image source={require('../assets/cicilan.png')} />
                                        <Text>Cicilan</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={{ justifyContent: 'center', alignItems: 'center' }}>
                                        <Image source={require('../assets/lainnya.png')} />
                                        <Text>Lainnya</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                        <View style={{ marginTop: wp('5%'), justifyContent: 'center', alignItems: 'center', borderRadius: 16 }}>
                            <View style={{ width: wp('90%'), borderRadius: 16}}>
                                <Slideshow
                                    dataSource={this.state.dataSource}
                                    position={this.state.position}
                                    onPositionChanged={position => this.setState({ position })} />
                            </View>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: hp('2%'), }}>
                            <ScrollView horizontal={true} contentContainerStyle={{ flexGrow: 1,  width: wp('200%')}}>
                                <View>
                                    <Image source={require('../assets/foto.png')} />
                                </View>
                                <View>
                                    <Image source={require('../assets/foto.png')} />
                                </View>
                                <View>
                                    <Image source={require('../assets/foto.png')} />
                                </View>
                                <View>
                                    <Image source={require('../assets/foto.png')} />
                                </View>

                            </ScrollView>
                        </View>
                        {/* <View style={{ height: 500 }} /> */}
                    </View>
                    </ScrollView>
               
            </View>
            
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

//make this component available to the app
export default Home;