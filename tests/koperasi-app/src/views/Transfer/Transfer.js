//import liraries
import React, { Component } from 'react';
import {
    View, Text, StyleSheet, StatusBar, Image, TextInput, KeyboardAvoidingView, ScrollView, TouchableOpacity
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Container, Header, Left, Body, Title } from 'native-base';
import Select2 from 'react-native-select-two';
import Icon from 'react-native-vector-icons/AntDesign';

const arrow = '../../assets/arrow.png';
// create a component
const Negara = [
    { id: 1, name: 'Indonesia' },
    { id: 2, name: 'Malaysia' },
    { id: 3, name: 'Singapur' }
];
const Bank = [
    { id: 1, name: 'BNI' },
    { id: 2, name: 'BCA' },
    { id: 3, name: 'Mandiri' }
];
const MataUang = [
    { id: 1, name: 'Rp' },
    { id: 2, name: 'USD' },
    { id: 3, name: 'SGD' },
    { id: 4, name: 'MYR' },
    { id: 5, name: 'AUD' },
];
class Transfer extends Component {
    render() {
        return (
            <Container>

                <StatusBar backgroundColor="#1018F2" barStyle="light-content" />
                {/* head */}
                <Header style={{ backgroundColor: '#1018F2' }} androidStatusBarColor='#1018F2'>
                    <TouchableOpacity>
                        <Left style={{
                            flexDirection: "row"
                        }}>
                            <Icon style={{ marginTop: wp("1%"), color: 'white' }} name='leftcircleo' size={24} color='white' />
                            <Title style={{ marginLeft: wp("2%"), fontSize: 20, color: 'white', marginTop: wp('1%') }}>Transfer</Title>
                        </Left>
                    </TouchableOpacity>
                    <Body>
                    </Body>
                </Header>
                <ScrollView style={{ flex: 1, backgroundColor: 'white' }} ref='scroll'>
                    <KeyboardAvoidingView style={styles.container} behavior={Platform.OS === "ios" ? "padding" : null} enabled>

                        <View style={{
                            padding: hp("2%"),
                            marginTop: hp("2%")
                        }}>
                            {/* pilih negara */}
                            <Text>Negara</Text>
                            <View style={{
                                marginTop: hp("2%")
                            }}>
                                <Select2
                                    isSelectSingle
                                    style={{ borderRadius: 5 }}
                                    colorTheme={'blue'}
                                    popupTitle='Select item'
                                    title='Select item'
                                    data={Negara}
                                    onSelect={data => {
                                        this.setState({ data });
                                    }}
                                    onRemoveItem={data => {
                                        this.setState({ data });
                                    }}
                                />
                            </View>
                            {/* end pilih negara */}

                            {/* end pilih Bank */}
                            <Text style={{
                                marginTop: hp("2%")
                            }}>Pilih Bank Tujuan</Text>
                            <View style={{
                                marginTop: hp("2%")
                            }}>
                                <Select2
                                    isSelectSingle
                                    style={{ borderRadius: 5 }}
                                    colorTheme={'blue'}
                                    popupTitle='Select item'
                                    title='Select item'
                                    data={Bank}
                                    onSelect={data => {
                                        this.setState({ data });
                                    }}
                                    onRemoveItem={data => {
                                        this.setState({ data });
                                    }}
                                />
                            </View>
                            {/* End Bank */}


                            {/* end pilih Mata Uang */}
                            <Text style={{
                                marginTop: hp("2%")
                            }}>Pilih Mata Uang</Text>
                            <View style={{
                                marginTop: hp("2%")
                            }}>
                                <Select2
                                    isSelectSingle
                                    style={{ borderRadius: 5 }}
                                    colorTheme={'blue'}
                                    popupTitle='Select item'
                                    title='Select item'
                                    data={MataUang}
                                    onSelect={data => {
                                        this.setState({ data });
                                    }}
                                    onRemoveItem={data => {
                                        this.setState({ data });
                                    }}
                                />
                            </View>
                            {/* end mata uang */}

                            {/* Nomor Rekening */}
                            <Text style={{
                                marginTop: hp("2%")
                            }}>Nomor rekening</Text>
                            <View style={{
                                marginTop: hp("2%")
                            }}>
                                <TextInput
                                    style={{
                                        height: 40,
                                        borderBottomWidth: 1
                                    }}
                                    placeholder="Input Nomor Rekening"
                                    onChangeText={(text) => this.setState({ text })}

                                />
                            </View>
                            {/* end mata uang */}
                            <Text style={{
                                marginTop: hp("2%"),
                            }}>Nominal Transfer</Text>
                            <View style={{
                                marginTop: hp("2%")
                            }}>
                                <TextInput
                                    style={{
                                        height: 40,
                                        borderBottomWidth: 1
                                    }}
                                    placeholder="Input Nomor Rekening"
                                    onChangeText={(text) => this.setState({ text })}
                                >{this.setState.data}</TextInput>
                            </View>
                        </View>
                        <View style={{
                            alignItems: "center",
                            justifyContent: "space-between"
                        }}>
                            <View style={{
                                width: wp("80%"),
                                height: hp("7%"),
                                backgroundColor: "#1018F2",
                                alignItems: "center",
                                justifyContent: "center",
                                borderRadius: 20
                            }}>
                                <Text style={{
                                    color: "white"
                                }}>Kirim</Text>
                            </View>
                        </View>
                    </KeyboardAvoidingView>
                </ScrollView>
            </Container>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
});

//make this component available to the app
export default Transfer;
