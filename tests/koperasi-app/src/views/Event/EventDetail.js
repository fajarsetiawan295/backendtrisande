//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, StatusBar, Image, ScrollView, KeyboardAvoidingView } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Container, Header, Left, Body, Title } from 'native-base';

const arrow = '../../assets/arrow.png';
const arow = '../../assets/foto.jpg';
// create a component
class EventDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataEvent: {
                id: "1",
                gambar: '../../assets/foto.jpg',
                judul: "Pelantikan Presiden-Wapres 2019-2024, Jokowi 'Undang' Prabowo",
                detail: "Joko Widodo berharap kepada Prabowo Subianto dan Sandiaga datang saat pelantikan Presiden dan Wakil Presiden 2019 terpilih pada 20 Oktober 2019 mendatang. Meskipun yang memiliki wewenang dalam mengundang Prabowo-Sandi pada pelantikan tersebut yaitu dari Majelis Permusyawaratan Rakyat (MPR). Pernyataan tersebut ia ucapkan saat berpidato usai ditetapkan sebagai Presiden dan Wakil Presiden RI periode 2019-2024 oleh Komisi Pemilihan Umum (KPU). Saya dan Pak Ma'ruf Amin akan sangat bahagia apabila Prabowo-Sandi datang pelantikan yang akan datang, ujar dia, di kantor KPU, Minggu 30/6/2019"
            }
        };
    }
    render() {
        return (
            <Container>

                <StatusBar backgroundColor="#1018F2" barStyle="light-content" />
                <Header style={{ backgroundColor: '#1018F2' }} androidStatusBarColor='#1018F2'>
                    <Left>
                        <View style={{
                            marginLeft: hp("1%")
                        }}>
                            <Image source={require(arrow)} />
                        </View>

                    </Left>
                    <Body>
                        <Title style={{ marginLeft: wp("5%"), fontSize: 24, color: 'white' }}>Details Event</Title>
                    </Body>
                </Header>
                <ScrollView style={{ flex: 1, backgroundColor: 'white' }} ref='scroll'>
                    <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>

                        <Image source={require(arow)} />
                        <View style={{
                            padding: hp("2%"),
                        }}>
                            <Text style={{
                                fontSize: 15,
                                fontWeight: "bold"
                            }}>{this.state.dataEvent.judul}</Text>
                        </View>
                        <View style={{
                            padding: hp("2%"),
                        }}>
                            <Text style={{
                                fontSize: 10,
                                textAlign: "justify"
                            }}>{this.state.dataEvent.detail}</Text>
                        </View>


                    </KeyboardAvoidingView>
                </ScrollView>
            </Container>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ffff',
    },
});

//make this component available to the app
export default EventDetails;
