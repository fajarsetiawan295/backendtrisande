//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, StatusBar, Image, ScrollView, KeyboardAvoidingView } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Container, Header, Left, Body, Title } from 'native-base';

const arrow = '../../assets/arrow.png';
// create a component
class RegisterOnBording extends Component {
    render() {
        return (
            <Container>

                <StatusBar backgroundColor="#1018F2" barStyle="light-content" />
                <Header style={{ backgroundColor: '#1018F2' }} androidStatusBarColor='#1018F2'>
                    <Left>
                        <View style={{
                            marginLeft: hp("1%")
                        }}>
                            <Image source={require(arrow)} />
                        </View>

                    </Left>
                    <Body>
                        <Title style={{ marginLeft: wp("5%"), fontSize: 24, color: 'white' }}>Details Event</Title>
                    </Body>
                </Header>
                <ScrollView style={{ flex: 1, backgroundColor: 'white' }} ref='scroll'>
                </ScrollView>
            </Container>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
});

//make this component available to the app
export default RegisterOnBording;
