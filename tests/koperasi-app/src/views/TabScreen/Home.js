import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, ScrollView } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Icon from 'react-native-vector-icons/FontAwesome';
import Slideshow from 'react-native-slideshow';
import RBSheet from "react-native-raw-bottom-sheet";
// import data from "../../components/static.json";
import FAIcon from "react-native-vector-icons/FontAwesome";
import MDIcon from "react-native-vector-icons/MaterialIcons";


FAIcon.loadFont();
MDIcon.loadFont();

// Icon Head
const icRegister = '../../assets/icon/register4.png';
const icFaceRecon = '../../assets/icon/facerecon4.png';
const icTransfer = '../../assets/icon/transfer4.png';
const icScan = '../../assets/icon/scan4.png';
const icDuit = '../../assets/icon/eduit4.png';

// icon Body
const icOpus = '../../assets/icon/calculate4.png';
const icMarket = '../../assets/icon/market2.png';
const icMaps = '../../assets/icon/map2.png';
const icTopUp = '../../assets/icon/health2.png';
const icPulsa = '../../assets/icon/phone2.png';
const icPdam = '../../assets/icon/air2.png';
const icGames = '../../assets/icon/game2.png';
const icPln = '../../assets/icon/electric2.png';
const icInternet = '../../assets/icon/signal2.png';
const icBpjs = '../../assets/icon/bpjs2.png';
const icCicilan = '../../assets/icon/handrp2.png';
const icMore = '../../assets/icon/more2.png';

// create a component
class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            position: 1,
            interval: null,
            dataSource: [
                {
                    title: 'Title 1',
                    caption: 'Caption 1',
                    url: 'http://placeimg.com/640/480/any',
                }, {
                    title: 'Title 2',
                    caption: 'Caption 2',
                    url: 'http://placeimg.com/640/480/any',
                }, {
                    title: 'Title 3',
                    caption: 'Caption 3',
                    url: 'http://placeimg.com/640/480/any',
                },
            ],
            grids: [
                {
                    label: "Upos",
                    uri: require('../../assets/icon/calculate4.png'),
                    color: "transparent"
                },
                {
                    label: "Upos",
                    uri: require('../../assets/icon/calculate4.png'),
                    color: "transparent"
                },
                {
                    label: "Upos",
                    uri: require('../../assets/icon/calculate4.png'),
                    color: "transparent"
                },
                {
                    label: "Upos",
                    uri: require('../../assets/icon/calculate4.png'),
                    color: "transparent"
                },

            ],
        };
    }
    componentWillMount() {
        this.setState({
            interval: setInterval(() => {
                this.setState({
                    position: this.state.position === this.state.dataSource.length ? 0 : this.state.position + 1
                });
            }, 2000)
        });
    }

    componentWillUnmount() {
        clearInterval(this.state.interval);
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.blueView}>
                    <View style={styles.headIcon}>
                        <TouchableOpacity>
                            <View style={{ marginLeft: wp('5%'), marginTop: wp('5%') }}>
                                <Icon name="user" color="white" size={30} />
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <View style={{ marginRight: wp('5%'), marginTop: wp('5%') }}>
                                <Icon name="cog" color="white" size={30} />
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <View style={styles.viewHeadName}>

                        </View>
                        <View style={styles.headTextName}>
                            <Text style={styles.textName}>Arief Advani</Text>
                            <View style={styles.boxSaldo}>
                                <View style={{ flexDirection: 'row', alignItems: 'flex-start' }}>
                                    <View>
                                        <Text style={{ color: '#919191' }}>Saldo{' '}
                                            <Text style={{ color: '#F4BE0C' }}>Rp 500.000</Text>
                                        </Text>
                                    </View>
                                </View>
                            </View>
                            <View style={styles.viewPoin}>
                                <Text style={styles.textPoint}>Poin{' '}
                                    <Text>0</Text>
                                </Text>
                            </View>
                        </View>
                        <View style={styles.buttonSaldo}>
                            <TouchableOpacity style={styles.touchSaldo}>
                                <Text>Isi Saldo</Text>
                            </TouchableOpacity>
                        </View>

                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                        <TouchableOpacity style={[styles.center, { marginLeft: wp('3%') }]}>
                            <Image source={require(icRegister)} style={styles.iconCycle} />
                            <Text style={styles.textIconHead}>Register</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.center}>
                            <Image source={require(icFaceRecon)} style={styles.iconCycle} />
                            <Text style={styles.textIconHead}>FR</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.center}>
                            <Image source={require(icScan)} style={styles.iconCycle} />
                            <Text style={styles.textIconHead}>Scan</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.center}>
                            <Image source={require(icTransfer)} style={styles.iconCycle} />
                            <Text style={styles.textIconHead}>Transfer</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.center, { marginRight: wp('3%') }]}>
                            <Image source={require(icDuit)} style={styles.iconCycle} />
                            <Text style={styles.textIconHead}>E-Duit</Text>
                        </TouchableOpacity>
                    </View>

                </View>
                <ScrollView style={styles.scrollView}>
                    <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: wp('5%') }}>
                        <View style={styles.boxShow}>

                            <View style={{ marginLeft: wp('3%'), marginTop: wp('2%') }}>
                                <Text style={{ fontSize: wp('5%') }}>Layanan</Text>
                                <Text>Lakukan pembayaran dengan cepat</Text>
                            </View>
                            <View style={styles.spaceAroundIcon}>
                                <TouchableOpacity style={styles.center}>
                                    <Image source={require(icOpus)} style={styles.iconCycle} />
                                    <Text>Upos</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.center}>
                                    <Image source={require(icMarket)} style={styles.iconCycle} />
                                    <Text>Pasar</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.center}>
                                    <Image source={require(icMaps)} style={styles.iconCycle} />
                                    <Text>Travel</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.center}>
                                    <Image source={require(icTopUp)} style={styles.iconCycle} />
                                    <Text>Top Up</Text>
                                </TouchableOpacity>
                            </View>
                            {/*  */}

                            <View style={styles.spaceAroundIcon}>
                                <TouchableOpacity style={styles.center}>
                                    <Image source={require(icPulsa)} style={styles.iconCycle} />
                                    <Text>Pulsa</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.center}>
                                    <Image source={require(icPdam)} style={styles.iconCycle} />
                                    <Text>PDAM</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.center}>
                                    <Image source={require(icGames)} style={styles.iconCycle} />
                                    <Text>Games</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.center}>
                                    <Image source={require(icPln)} style={styles.iconCycle} />
                                    <Text>PLN</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={styles.spaceAroundIcon}>
                                <TouchableOpacity style={styles.center}>
                                    <Image source={require(icInternet)} style={styles.iconCycle} />
                                    <Text>Internet</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.center}>
                                    <Image source={require(icBpjs)} style={styles.iconCycle} />
                                    <Text>BPJS</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.center}>
                                    <Image source={require(icCicilan)} style={styles.iconCycle} />
                                    <Text>Cicilan</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => this.Scrollable.open()} style={styles.center}>
                                    <Image source={require(icMore)} style={styles.iconCycle} />
                                    <Text>Lainnya</Text>
                                    <RBSheet
                                        height={500}
                                        ref={ref => {
                                            this.Scrollable = ref;
                                        }}
                                        closeOnDragDown
                                        customStyles={{
                                            container: {
                                                borderTopLeftRadius: 10,
                                                borderTopRightRadius: 10
                                            }
                                        }}
                                    >
                                        <ScrollView>
                                            <View style={styles.gridContainer}>
                                                {this.state.grids.map(grid => (
                                                    <TouchableOpacity
                                                        // key={grid.icon}
                                                        onPress={() => this.Scrollable.close()}
                                                        style={styles.gridButtonContainer}
                                                    >
                                                        <View style={[styles.gridButton, { backgroundColor: grid.color }]}>
                                                            <Image source={grid.uri} style={styles.gridIcon} />
                                                        </View>
                                                        <Text style={styles.gridLabel}>{grid.label}</Text>
                                                    </TouchableOpacity>
                                                ))}
                                            </View>
                                        </ScrollView>
                                    </RBSheet>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={styles.viewSlideshow}>
                            <View style={{ width: wp('90%'), borderRadius: 16 }}>
                                <Slideshow
                                    // containerStyle={{borderRadius: 30}}
                                    dataSource={this.state.dataSource}
                                    position={this.state.position}
                                    onPositionChanged={position => this.setState({ position })} />
                            </View>
                        </View>
                    </View>
                    <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 5 }}>
                        <ScrollView horizontal={true} style={{ width: wp('90%'), height: hp('40%') }}>
                            <View style={{ marginRight: 5, borderRadius: 10, borderBottomLeftRadius: 10, borderBottomRightRadius: 10 }}>
                                <Image source={require('../../assets/home/Blog_Judul-Blog-hdpi.png')} style={{ width: wp('90%'), height: hp('30%'), borderRadius: 10, borderBottomLeftRadius: 10, borderBottomRightRadius: 10 }} />
                            </View>
                            <View style={{ marginRight: 5 }}>
                                <Image source={require('../../assets/home/Blog_Judul-Blog-hdpi.png')} style={{ width: wp('90%'), height: hp('30%'), borderRadius: 10 }} />
                            </View>
                        </ScrollView>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    blueView: {
        width: wp('100%'),
        backgroundColor: '#1464CC',
        height: hp('40%'),
        zIndex: 1,
        borderBottomLeftRadius: 5,
        borderBottomRightRadius: 5
    },
    headIcon: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    viewHeadName: {
        width: wp('20%'),
        height: wp('20%'),
        borderRadius: wp('20%'),
        backgroundColor: 'white',
        marginLeft: wp('5%'),
        marginTop: wp('5%')
    },
    headTextName: {
        width: wp('20%'),
        height: wp('20%'),
        borderRadius: wp('20%'),
        marginTop: wp('5%'),
        marginRight: wp('20%')
    },
    textPoint: {
        color: 'white',
        fontWeight: '600',
        fontSize: wp('4%')
    },
    textName: {
        color: 'white',
        width: wp('40%'),
        fontWeight: '600',
        fontSize: wp('4%')
    },
    buttonSaldo: {
        width: wp('20%'),
        height: wp('20%'),
        borderRadius: wp('20%'),
        marginRight: wp('5%'),
        marginTop: wp('10.6%')
    },
    touchSaldo: {
        width: wp('20%'),
        height: hp('5%'),
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: wp('1%')
    },
    viewPoin: {
        flexDirection: 'row',
        marginTop: wp('1%')
    },
    scrollView: {
        width: "100%",
        backgroundColor: 'white',
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    iconCycle: {
        width: 50,
        height: 50
    },
    textIconHead: {
        color: 'white',
        fontWeight: '600'
    },
    boxShow: {
        backgroundColor: 'white',
        width: wp('90%'),
        height: hp('50%'),
        borderRadius: wp('5%'),
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
        justifyContent: 'space-around',
        position: 'relative',
        zIndex: 0
        // marginTop: hp('50%')
    },
    viewSlideshow: { 
        marginTop: wp('5%'), 
        justifyContent: 'center', 
        alignItems: 'center', 
        borderRadius: 16 
    },
    boxSaldo: {
        width: wp('40%'),
        height: hp('5%'),
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: wp('1%')
    },
    spaceAroundIcon: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginLeft: wp('1%'),
        marginRight: wp('1%')
    },
    gridContainer: {
        flex: 1,
        flexDirection: "row",
        flexWrap: "wrap",
        padding: 20,
        marginBottom: 20,

    },
    gridButtonContainer: {
        flexBasis: "25%",
        marginTop: 20,
        justifyContent: "center",
        alignItems: "center"
    },
    gridButton: {
        width: 50,
        height: 50,
        borderRadius: 25,
        justifyContent: "center",
        alignItems: "center"
    },
    gridIcon: {
        width: wp('15%'),
        height: wp('15%'),
        fontSize: 30,
        color: "white"
    },
    gridLabel: {
        fontSize: 14,
        paddingTop: 10,
        color: "#333"
    },
});

//make this component available to the app
export default Home;
