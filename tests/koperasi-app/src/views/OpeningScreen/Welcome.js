import React, { Component } from 'react';
import { View, YellowBox, StatusBar, AsyncStorage, Dimensions } from 'react-native';
import { Container } from 'native-base';
import AppIntroSlider from 'react-native-app-intro-slider';
import Ionicons from 'react-native-vector-icons/Ionicons';

const GLOBAL = require('../../config/Service');
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

/**
 * Supress the warning
 */
YellowBox.ignoreWarnings(['Warning: ViewPagerAndroid has been extracted from react-native core']);

class Welcome extends Component {
    constructor(props) {
        super(props);
        this.state = {
            slides: [
                {
                    key: 'k1',
                    image: require('../../assets/demoonline.png'),
                    imageStyle: {
                        flex:1, width:width/1, height:height/1
                    },
                },
                {
                    key: 'k2',
                    image: require('../../assets/demoonline.png'),
                    imageStyle: {
                        flex:1, width:width/1, height:height/1
                    },
                },
                {
                    key: 'k3',
                    image: require('../../assets/demoonline.png'),
                    imageStyle: {
                        flex:1, width:width/1, height:height/1
                    },
                }
            ]
        };
    }

    async componentDidMount() {
        const WELCOME = await AsyncStorage.getItem('WELCOME');
        if (WELCOME !== null) {
            this.props.navigation.navigate('SplashScreen');
        }
    }

    renderNextButton = () => {
        return (
            <View style={{ width: 40, height: 40, backgroundColor: 'rgba(0, 0, 0, .2)', borderRadius: 20, justifyContent: 'center', alignItems: 'center' }}>
                <Ionicons
                    name='md-arrow-round-forward'
                    color='rgba(255, 255, 255, .9)'
                    size={24}
                    style={{ backgroundColor: 'transparent' }}
                />
            </View>
        );
    }

    renderDoneButton = () => {
        return (
            <View style={{ width: 40, height: 40, backgroundColor: 'rgba(0, 0, 0, .2)', borderRadius: 20, justifyContent: 'center', alignItems: 'center' }}>
                <Ionicons
                    name='md-checkmark'
                    color='rgba(255, 255, 255, .9)'
                    size={24}
                    style={{ backgroundColor: 'transparent' }}
                />
            </View>
        );
    }

    onDonePress = () => {
        this.props.navigation.navigate('SplashScreen');
    }

    render() {
        return (
            <Container>
                <StatusBar
                    backgroundColor='#00BBFA'
                    barStyle='light-content'
                />
                <AppIntroSlider
                    slides={this.state.slides}
                    renderNextButton={this.renderNextButton}
                    renderDoneButton={this.renderDoneButton}
                    onDone={this.onDonePress}
                />
            </Container>
        );
    }
}

export default Welcome;
