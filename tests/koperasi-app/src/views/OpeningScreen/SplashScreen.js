import React, { Component } from 'react';
import { View, ImageBackground, StatusBar, AsyncStorage } from 'react-native';

const GLOBAL = require('../../config/Service');

class SplashScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    async componentDidMount() {
        this._isMounted = true;
        await AsyncStorage.setItem(GLOBAL.WELCOME_SCREEN, JSON.stringify('welcome_success'));
        await AsyncStorage.getItem(GLOBAL.APPUSERID).then((value)=>{
            if (value !== null) {
                setTimeout(() => {
                    this.props.navigation.navigate('AppMainSwitchNavigator');
                }, 2000);
            } else {
                setTimeout(() => {
                    this.props.navigation.navigate('AppAuthentication');
                }, 2000);
            }
        });
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <StatusBar
                    backgroundColor='#00BBFA'
                    barStyle='light-content'
                />
                <ImageBackground source={require('../../assets/Splashscreen.jpeg')} style={{ flex: 1 }}>
                </ImageBackground>
            </View>
        );
    }
}

export default SplashScreen;
