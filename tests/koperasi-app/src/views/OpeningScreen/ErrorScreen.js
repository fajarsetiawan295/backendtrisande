import React, { Component } from 'react';
import { View, Text, Image, Dimensions, TouchableOpacity, StatusBar } from 'react-native';
import { Container } from 'native-base';

const { width, height } = Dimensions.get('window');

class ErrorScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <Container>
                <StatusBar
                    backgroundColor='#00BBFA'
                    barStyle='light-content'
                />
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center',}}>
                    <Image source={require('../../assets/error-image.png')} style={{ width: (width) / 3, height: (width) / 3, resizeMode: 'contain' }} />
                    <View style={{ marginTop: 20, alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ fontSize: 24, fontWeight: '500', color: '#333' }}>Ooppss...</Text>
                        <Text style={{ fontSize: 24, fontWeight: '400', color: '#333', textAlign: 'center' }}>Kami mendeteksi adanya kesalahan.</Text>
                    </View>
                    <View style={{ marginTop: 10, alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ fontSize: 15, fontWeight: '400', color: 'grey', textAlign: 'center' }}>- Pastikan koneksi internet kamu menyala.</Text>
                        <Text style={{ fontSize: 15, fontWeight: '400', color: 'grey', textAlign: 'center' }}>- Matikan penggunaan VPN jika kamu menggunakan VPN.</Text>
                        <Text style={{ fontSize: 15, fontWeight: '400', color: 'grey', textAlign: 'center' }}>- Jalankan ulang aplikasi Ini.</Text>
                    </View>
                    <View style={{ marginTop: 20, alignItems: 'center', justifyContent: 'center' }}>
                        <TouchableOpacity style={{ width: (width) / 2, height: (width) / 10, alignItems: 'center', justifyContent: 'center', backgroundColor: '#00BBFA', borderRadius: 10 }} onPress={() => this.props.navigation.navigate('BlankScreen')}>
                            <Text style={{ fontSize: 24, fontWeight: '500', color: 'white' }}>Mulai Ulang</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </Container>
        );
    }
}

export default ErrorScreen;
