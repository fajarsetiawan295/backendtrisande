import React, { Component } from 'react';
import { View, AsyncStorage } from 'react-native';
const GLOBAL = require('../../config/Service');

class BlankScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    async componentDidMount() {
        await AsyncStorage.getItem(GLOBAL.WELCOME_SCREEN).then((value)=>{
            if (value === null) {
                this.props.navigation.navigate('Welcome');
            } else {
                this.props.navigation.navigate('SplashScreen');
            }
        });
    }

    render() {
        return (
            <View />
        );
    }
}

export default BlankScreen;
