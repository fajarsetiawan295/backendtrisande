// Icon Head
export { default as icRegister } from '../assets/icon/register4.png';
export { default as icFaceRecon } from '../assets/icon/facerecon4.png';
export { default as icTransfer } from '../assets/icon/transfer4.png';
export { default as icScan } from '../assets/icon/scan4.png';
export { default as icDuit } from '../assets/icon/eduit4.png';

// icon Body
export { default as icOpus } from '../assets/icon/calculate4.png';
export { default as icMarket } from '../assets/icon/market2.png';
export { default as icMaps } from '../assets/icon/map2.png';
export { default as icTopUp } from '../assets/icon/health2.png';
export { default as icPulsa } from '../assets/icon/phone2.png';
export { default as icPdam } from '../assets/icon/air2.png';
export { default as icGames } from '../assets/icon/game2.png';
export { default as icPln } from '../assets/icon/electric2.png';
export { default as icInternet } from '../assets/icon/signal2.png';
export { default as icBpjs } from '../assets/icon/bpjs2.png';
export { default as icCicilan } from '../assets/icon/handrp2.png';
export { default as icMore } from '../assets/icon/more2.png';