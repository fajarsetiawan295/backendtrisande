import { configureFonts } from 'react-native-paper';
export const fontConfig = {
    // default: {
    //   regular: {
    //     fontFamily: 'DINPro',
    //     fontWeight: 'normal',
    //   },
    //   medium: {
    //     fontFamily: 'DINPro',
    //     fontWeight: 'normal',
    //   },
    //   light: {
    //     fontFamily: 'DINPro',
    //     fontWeight: 'normal',
    //   },
    //   thin: {
    //     fontFamily: 'DINPro',
    //     fontWeight: 'normal',
    //   },
    // },
  };
  
export const theme = {
    fonts: configureFonts(fontConfig),
    roundness: 14,
    colors: {
        primary: 'grey',
        underlineColor: 'transparent',
    }
}