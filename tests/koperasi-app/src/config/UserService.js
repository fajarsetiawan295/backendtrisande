const GLOBAL = require('./Service');
import moment from 'moment';

export const USER_LOGIN = async (username, password) => {
    // console.log(username, password)
    try {
        var tempData = new FormData();
        tempData.append('username', username);
        tempData.append('password', password);
        const postUrl = await fetch(GLOBAL.LOGIN_URL, {
            method: 'POST',
            body: tempData,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data'
            },
            body:tempData
            // body: JSON.stringify({
            //     username: email,
            //     password: password
            // })
        });
        const resJson = await postUrl.json();
        return resJson;
    } catch (err) {
        return err;
    }
}

export const USER_REGISTRASI = async (username, password, name, email, phone, address, city, gender, birth) => {
    try {
        var tempData = new FormData();
        tempData.append('username', username);
        tempData.append('password', password);
        tempData.append('name', name);
        tempData.append('email', email);
        tempData.append('phone', phone);
        tempData.append('address', address);
        tempData.append('city', city);
        tempData.append('gender', gender);
        tempData.append('birth', moment(birth).format('YYYY-MM-DD'));
        const postUrl = await fetch(GLOBAL.REGITRASI_URL, {
            method: 'POST',
            body: tempData,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data'
            },
            body:tempData
        });
        const resJson = await postUrl.json();
        return resJson;
    } catch (err) {
        return err;
    }
}

export const USER_INFO = async (user_id) => {
    // console.log(username, password)
    try {
        var tempData = new FormData();
        tempData.append('user_id', user_id);
        const postUrl = await fetch(GLOBAL.USERINFO_URL, {
            method: 'POST',
            // body: tempData,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data'
            },
            body:tempData
            // body: JSON.stringify({
            //     username: email,
            //     password: password
            // })
        });
        const resJson = await postUrl.json();
        // console.log('kontol anjing', resJson);
        return resJson;
    } catch (err) {
        return err;
    }
}
export const USER_PROFILE = async (user_id) => {
    // console.log(username, password)
    try {
        var tempData = new FormData();
        tempData.append('user_id', user_id);
        const postUrl = await fetch(GLOBAL.USERPROFILE_URL, {
            method: 'POST',
            body: tempData,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data'
            },
            body:tempData
            // body: JSON.stringify({
            //     username: email,
            //     password: password
            // })
        });
        const resJson = await postUrl.json();
        return resJson;
    } catch (err) {
        return err;
    }
}