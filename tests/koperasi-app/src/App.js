//import liraries
import React, { Component } from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { View, Image, Dimensions } from 'react-native';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

//Opening Screen
import BlankScreen from './views/OpeningScreen/BlankScreen';
import Welcome from './views/OpeningScreen/Welcome';
import SplashScreen from './views/OpeningScreen/SplashScreen';
import ErrorScreen from './views/OpeningScreen/ErrorScreen';

//Tab Screen
import Home from './views/TabScreen/Home';
import Inbox from './views/TabScreen/Inbox';
import Chat from './views/TabScreen/Chat';
import Call from './views/TabScreen/Call';

//Auth Screen
import Login from './views/Auth/Login';
import Register from './views/Auth/Register';

const AppBottomTabNavigator = createBottomTabNavigator({
    Home: {
        screen: Home,
        navigationOptions: {
            tabBarIcon: ({ tintColor }) => (
                // <Ionicons name={'md-home'} size={30} color={tintColor} />
                <Image source={require('./assets/tabBottom/Icon_open-home-xxxhdpi.png')} style={{width: wp('6%'), height: wp('6%'), color: "red"}}/>
                
            )
        }
    },
    Inbox: {
        screen: Inbox,
        navigationOptions: {
            tabBarIcon: ({ tintColor }) => (
                <Ionicons name={'md-book'} size={30} color={tintColor} />
                // <Image source={require('./assets/tabBottom/')} style={{width: wp('6%'), height: wp('6%')}}/>
            )
        }
    },
    Chat: {
        screen: Chat,
        navigationOptions: {
            tabBarIcon: ({ tintColor }) => (
                <Ionicons name={'md-desktop'} size={30} color={tintColor} />
            )
        }
    },
    Call: {
        screen: Call,
        navigationOptions: {
            tabBarIcon: ({ tintColor }) => (
                // <EntypoIcon name='menu' color={tintColor} size={30} />
                <Ionicons name={'md-notifications'} size={30} color={tintColor} />
            )
        }
    }
}, {
    // initialRouteName: 'Home',
    tabBarOptions: {
        showLabel: false,
        activeTintColor: 'white',
        inactiveTintColor: 'grey',
        style: {
            backgroundColor: '#1464CC'
        }
    },
   
});

const AppMainSwitchNavigator = createSwitchNavigator({
    AppBottomTabNavigator: AppBottomTabNavigator,
    // AppEbookSwitchNavigator: AppEbookSwitchNavigator,
});

const AppAuthentication = createSwitchNavigator({
    Login: { screen: Login },
    // ForgotPassword: { screen: ForgotPassword },
    Register: { screen: Register },
})

const AppHomeSwitchNavigator = createSwitchNavigator({
    Home: { screen: Home}
})

const AppAuthException = createSwitchNavigator({
    BlankScreen: BlankScreen,
    ErrorScreen: ErrorScreen,
    Welcome: Welcome,
    SplashScreen: SplashScreen,
    AppAuthentication: AppAuthentication,
    AppMainSwitchNavigator: AppMainSwitchNavigator,
    // AppDrawerNavigator: AppDrawerNavigator,
}, {
    initialRouteName: 'BlankScreen'
});
export default createAppContainer(AppAuthException);