<?php

use Illuminate\Http\Request;

Route::group([
    'prefix' => 'user',
], function () {
    Route::post('register', 'UserController@register');
    Route::post('login', 'UserController@login');
    Route::post('userInfo', 'UserController@userInfo');
    Route::post('profile', 'UserController@profile');
    Route::get('ebook_free', 'ebook@ebook_free');
    Route::post('upload_ebook_free', 'ebook@upload_ebook_free');
    Route::post('detail_books', 'ebook@detail_books');
    Route::post('order_ebook_free', 'ebook@order_ebook_free');
    Route::post('detail_user_books_free', 'ebook@detail_user_books_free');
    Route::post('rating_books_free', 'ebook@rating_books_free');
    Route::get('getreting/{id_ebook_free}', 'ebook@getreting');
    Route::post('gendre', 'ebook@gendre');
    Route::get('listgendre', 'ebook@listgendre');
    Route::post('upload_video_free', 'video@upload_video_free');
    Route::post('detail_video', 'video@detail_video');
    Route::get('video_free', 'video@video_free');
    Route::post('order_video_free', 'video@order_video_free');
    Route::post('detail_video_books_free', 'video@detail_video_books_free');
});
