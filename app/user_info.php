<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class user_info extends Model
{
    protected $table = 'user_info';
    protected $fillable = [
        'walet', 'id', 'walet', 'id_walet', 'id_user'
    ];
    public function user_info()
    {
        return $this->hasOne(user_info::class);
    }
}
