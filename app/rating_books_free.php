<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class rating_books_free extends Model
{
    protected $table = 'rating_ebook_free';
    protected $fillable = [
        'id_user', 'nilai_rating', 'id_ebook_free'
    ];
}
