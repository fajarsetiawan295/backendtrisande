<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class list_buku_user_free extends Model
{
    protected $table = 'list_buku_user_free';
    protected $fillable = [
        'id_buku', 'id_user'
    ];
    public function ebook_free()
    {
        return $this->belongsTo(ebook_free::class);
    }
}
