<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class books_gendre extends Model
{
    protected $table = 'books_gendre';
    protected $primaryKey = 'id_genre';
    protected $fillable = [
        'nama', 'gambar_img'
    ];
}
