<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class list_video_user_list extends Model
{
    protected $table = 'list_video_user_free';
    protected $fillable = [
        'id_video', 'id_user'
    ];
    public function ebook_free()
    {
        return $this->belongsTo(ebook_free::class);
    }
}
