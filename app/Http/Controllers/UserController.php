<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\Profile;
use Illuminate\Http\Request;
use App\User;
use App\user_info;
use App\ebook_free;
use Auth;
use Validator;

class UserController extends Controller
{
    public function register(Request $request)
    {
        Validator::make($request->all(), [
            'username' => 'required|string',
            'password' => 'required|string',
            'name' => 'requierd|string',
            'email' => 'required|email',
            'phone' => 'required',
            'birth' => 'required|string',
            'address' => 'required|string',
            'city' => 'required|string',
            'gender' => 'required|string'
        ]);

        $user_exists = User::where('username', $request->username)->first();
        if (!$user_exists) {
            $user = User::create([
                'username' => $request->username,
                'email' => $request->email,
                'password' => bcrypt($request->password)
            ]);

            if ($user) {
                //Creaeting profile data
                $user->profile()->create([
                    'name' => $request->name,
                    'address' => $request->address,
                    'city' => $request->city,
                    'phone' => $request->phone,
                    'birth' => $request->birth,
                    'gender' => $request->gender,
                    'photo_profile' => ''

                ]);
                user_info::create([
                    'id' =>  $user->id,
                    'walet' => 0,
                    'id_walet' => $user->id,
                    'id_user' => $user->id,
                ]);
                return response()->json([
                    'status' => 200,
                    'message' => 'Successfully registered'
                ], 200);
            }
        } else {
            //User already exists
            return response()->json([
                'status' => 205,
                'message' => 'Username already exists'
            ], 205);
        }
    }

    public function login(Request $request)
    {
        $request->validate([
            'username' => 'required|string',
            'password' => 'required|string',
        ]);

        $credentials = request(['username', 'password']);
        if (!Auth::attempt($credentials)) {
            return response()->json([
                'status' => 401,
                'message' => 'Username or password is wrong!'
            ], 401);
        }

        $user = User::with('profile')->where('username', $request->username)->first();
        $id = User::find($user->id);
        $data = [
            'id' =>  $id->id,
        ];
        return response()->json([
            'status' => 200,
            'message' => 'Successfully login',
            'data' =>  $data,
        ], 200);
    }


    public function userInfo(Request $request)
    {
        $user = Profile::where('user_id', $request->user_id)->first();
        $info = user_info::find($user->user_id);

        $data = [
            'name' => $user->name,
            'walet' => $info->walet,
            'photo_profile' => $user->photo_profile,
        ];

        return response()->json([
            'status' => 200,
            'message' => 'Successfully get user info',
            'data' =>  $data,
        ], 200);
    }
    public function profile(Request $request)
    {
        $user = Profile::where('user_id', $request->user_id)->first();
        if ($user === null) {
            return response()->json([
                'status' => 200,
                'message' => 'data User tidak di temukan',
            ], 200);
        } else {
            return response()->json([
                'status' => 200,
                'message' => 'Successfully get Profile Info ',
                'data' =>  $user,
            ], 200);
        }
    }
}
