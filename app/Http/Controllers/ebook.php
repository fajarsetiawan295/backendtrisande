<?php

namespace App\Http\Controllers;

use DB;
use App\Quotation;

use Illuminate\Http\Request;
use App\Http\Resources\UserResource;
use App\Profile;
use App\User;
use Auth;
use Validator;
use App\Http\Resources\DataBukufree;
use App\Http\Resources\DataBukudetail;
use App\ebook_free;
use App\list_buku_user_free;
use App\rating_books_free;
use App\books_gendre;
use App\Http\Resources\listgendre;
use Illuminate\Http\Response;

class ebook extends Controller
{
    //ebook freeee
    public function ebook_free()
    {
        $hasil = ebook_free::all();
        return response()->json([
            'status' => 200,
            'message' => 'data terambil',
            'data' => DataBukudetail::collection(collect($hasil))
        ], 200);
    }

    public function detail_books(Request $request)
    {
        //  $user = ebook_free::where('id', $request->id)->first();
        $hasil = ebook_free::find($request->id);
        return response()->json([
            'status' => 200,
            'message' => 'data terambil',
            'data' => [
                'id' => $hasil->id,
                'nama_buku' => $hasil->nama_buku,
                'description_buku' => $hasil->description_buku,
                'img_cover' => url('/') . $hasil->img_cover,
                'file_buku' => url('/') . $hasil->file_buku,
                'created_at' => $hasil->created_at,
                'updated_at' => $hasil->updated_at,
            ]
        ], 200);
    }
    public function upload_ebook_free(Request $request)
    {
        $request->validate([
            'nama_buku' => 'required',
            'description_buku' => 'required',
            'img_cover' => 'required',
            'file_buku' => 'required',
        ]);

        $file = $request->file('img_cover');
        $imagePath = '/assets/img/';
        $path = public_path() . $imagePath;
        $extension = $file->getClientOriginalExtension();
        $filename = 'event-' . time() . '.' . $extension;
        $request->file('img_cover')->move($path, $filename);


        $pdf = $request->file('file_buku')->getClientOriginalName();
        $pdfPath = '/assets/pdf/';
        $urlpdf = public_path() . $pdfPath;
        $extension = $request->file('file_buku')->getClientOriginalExtension();
        $pdfname = 'pdf-' . time() . '.' . $extension;
        $request->file('file_buku')->move($urlpdf, $pdfname);


        $u = ebook_free::create([
            'nama_buku' => $request->nama_buku,
            'description_buku' => $request->description_buku,
            'img_cover' => $imagePath . $filename,
            'file_buku' => $pdfPath . $pdfname,
        ]);
        if ($u === null) {
            return response()->json([
                'status' => 204,
                'message' => 'upload gagal',
            ], 200);
        } else {
            return response()->json([
                'status' => 200,
                'message' => 'Successfully upload data ',
                'data' =>  $u,
            ], 200);
        }
    }
    public function order_ebook_free(Request $request)
    {
        $request->validate([
            'id_user' => 'required|string',
            'id_buku' => 'required|string',
        ]);
        $buku_exists =   DB::select('select * from list_buku_user_free where id_user= ' . $request->id_user . ' and id_buku= ' . $request->id_buku . '');

        if (!$buku_exists) {
            $u = list_buku_user_free::create([
                'id_user' => $request->id_user,
                'id_buku' => $request->id_buku,
            ]);
            if ($u === null) {
                return response()->json([
                    'status' => 204,
                    'message' => 'data User tidak di temukan',
                ], 201);
            } else {
                return response()->json([
                    'status' => 200,
                    'message' => 'Successfully',
                    'data' =>  $u,
                    //  'dat' => $buku_exists
                ], 200);
            }
        } else {
            return response()->json([
                'status' => 405,
                'message' => 'kamu sudah membeli buku ini',
                //  'data' => $buku_exists
            ], 201);
        }
    }
    public function detail_user_books_free(Request $request)
    {
        // $user = list_buku_user_free::with('ebook_free')->where('id_user', $request->id_user)->get();
        $user = DB::table('list_buku_user_free')
            ->join('ebook_free', 'list_buku_user_free.id_buku', '=', 'ebook_free.id')
            ->where('list_buku_user_free.id_user', '=', $request->id_user)
            ->select('ebook_free.*', 'list_buku_user_free.*')
            ->get();

        if ($user === null) {
            return response()->json([
                'status' => 200,
                'message' => 'data User tidak di temukan',
            ], 200);
        } else {
            return response()->json([
                'status' => 200,
                'message' => 'Successfully get data buku ',
                'id_user' => DataBukufree::collection(collect($user)),
                //     'buku' =>  $buku,
            ], 200);
        }
    }
    public function rating_books_free(Request $request)
    {
        $request->validate([
            'id_user' => 'required|string',
            'nilai_rating' => 'required|string',
            'id_ebook_free' => 'required|string',
        ]);

        $rating =  DB::select('select * from rating_ebook_free where id_user= ' . $request->id_user . ' and id_ebook_free= ' . $request->id_ebook_free . '');
        if (!$rating) {
            $hasil =  rating_books_free::create([
                'id_user' => $request->id_user,
                'id_ebook_free' => $request->id_ebook_free,
                'nilai_rating' => $request->nilai_rating
            ]);
            if ($hasil) {
                return response()->json([
                    'status' => 200,
                    'message' => 'berhasil menambahkan reting',
                    'data_buku' => $hasil
                ], 200);
            } else {
                return response()->json([
                    'status' => 201,
                    'message' => 'gagal menambahkan rating'
                ], 201);
            }
        } else {
            $rating =  DB::select('UPDATE rating_ebook_free SET nilai_rating=' . $request->nilai_rating . ' WHERE  id_user=' . $request->id_user . ' AND id_ebook_free=' . $request->id_ebook_free . '');
            $data = [
                'id_user' => $request->id_user,
                'id_ebook_free' => $request->id_ebook_free,
                'nilai_rating' => $request->nilai_rating,

            ];
            if ($rating) {
                return response()->json([
                    'status' => 200,
                    'message' => 'berhasil update reting',
                    'data_buku' => $data
                ], 200);
            } else {
                return response()->json([
                    'status' => 201,
                    'message' => 'gagal menambahkan rating'
                ], 201);
            }
        }
    }
    public function getreting($id_ebook_free)
    {
        $hasil = rating_books_free::where('id_ebook_free', $id_ebook_free)->get();

        $user = DB::table('rating_ebook_free')
            ->join('users', 'rating_ebook_free.id_ebook_free', '=', 'users.id')
            ->where('rating_ebook_free.id_ebook_free', '=', $id_ebook_free)
            ->select('users.username', 'rating_ebook_free.nilai_rating')
            ->get();

        $count = rating_books_free::where('id_ebook_free', $id_ebook_free)->count();

        if ($count === 0) {

            return Response()->json([
                'status' => 201,
                'Buku_rating' => 'belum ada reting',

            ], 201);
        } else {

            $price = DB::table('rating_ebook_free')
                ->where('id_ebook_free', $id_ebook_free)
                ->sum('nilai_rating');
            $sum = $price / $count;
            return Response()->json([
                'status' => 200,
                'total' => $price,
                'jumlah_user_rating' => $count,
                'hasil_rating' => $sum,
                'user_rating' => $user,


            ], 200);
        }
    }
    //end ebook free


    //gendre 


    //gendre upload
    public function gendre(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'gambar_img' => 'required',
        ]);

        $file = $request->file('gambar_img');
        $imagePath = '/assets/img/';
        $path = public_path() . $imagePath;
        $extension = $file->getClientOriginalExtension();
        $filename = 'gendre-' . time() . '.' . $extension;
        $request->file('gambar_img')->move($path, $filename);

        $gender_exists = books_gendre::where('nama', $request->nama)->first();
        if (!$gender_exists) {
            $input =  books_gendre::create([
                'nama' => $request->nama,
                'gambar_img' => $imagePath . $filename,
            ]);

            if (!$input) {
                return response()->json([
                    'status' => 201,
                    'message' => 'gagal menambahkan gender',
                ], 201);
            } else {
                return response()->json([
                    'status' => 200,
                    'message' => 'Berhasil Menambahkan Gendre',
                ], 200);
            }
        } else {
            return response()->json([
                'status' => 201,
                'message' => 'Data gendre sudah ada',
            ], 201);
        }
    }
    //list gendre
    public function listgendre()
    {
        $hasil = books_gendre::all();
        return response()->json([
            'status' => 200,
            'message' => 'data terambil',
            'data' => listgendre::collection(collect($hasil))
        ], 200);
    }

    //nambah buku 
}
