<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\video_free;
use App\Http\Resources\VideoDetail;
use App\Http\Resources\Datavideofree;
use App\list_video_user_list;

class video extends Controller
{
    public function video_free()
    {
        $hasil = video_free::all();
        return response()->json([
            'status' => 200,
            'message' => 'data terambil',
            'data' => VideoDetail::collection(collect($hasil))
        ], 200);
    }

    public function upload_video_free(Request $request)
    {
        $request->validate([
            'nama_video' => 'required',
            'description_video' => 'required',
            'video_cover' => 'required',
            'file_video' => 'required',
        ]);

        $file = $request->file('video_cover');
        $imagePath = '/assets/img/';
        $path = public_path() . $imagePath;
        $extension = $file->getClientOriginalExtension();
        $filename = 'event-' . time() . '.' . $extension;
        $request->file('video_cover')->move($path, $filename);


        $pdf = $request->file('file_video')->getClientOriginalName();
        $videopath = '/assets/video/';
        $urlvideo = public_path() . $videopath;
        $extension = $request->file('file_video')->getClientOriginalExtension();
        $videoname = 'video-' . time() . '.' . $extension;
        $request->file('file_video')->move($urlvideo, $videoname);


        $u = video_free::create([
            'nama_video' => $request->nama_video,
            'description_video' => $request->description_video,
            'video_cover' => $imagePath . $filename,
            'file_video' => $videopath . $videoname,
        ]);
        if ($u === null) {
            return response()->json([
                'status' => 204,
                'message' => 'upload gagal',
            ], 200);
        } else {
            return response()->json([
                'status' => 200,
                'message' => 'Successfully upload data ',
                'data' =>  $u,
            ], 200);
        }
    }
    public function detail_video(Request $request)
    {
        //  $user = ebook_free::where('id', $request->id)->first();
        $hasil = video_free::find($request->id);
        if (!$hasil) {
            return response()->json([
                'status' => 202,
                'message' => 'data tidak terambil',
                'data' => $hasil
            ], 202);
        } else {
            return response()->json([
                'status' => 200,
                'message' => 'data terambil',
                'data' =>
                [
                    'id' => $hasil->id,
                    'nama_video' => $hasil->nama_video,
                    'description_video' => $hasil->description_video,
                    'video_cover' => url('/') . $hasil->video_cover,
                    'file_video' => url('/') . $hasil->file_video,
                    'created_at' => $hasil->created_at,
                    'updated_at' => $hasil->updated_at,
                ]
            ], 200);
        }
    }

    public function order_video_free(Request $request)
    {
        $request->validate([
            'id_video' => 'required|string',
            'id_user' => 'required|string',
        ]);
        $buku_exists =   DB::select('select * from list_video_user_free where id_video= ' . $request->id_video . ' and id_user= ' . $request->id_user . '');

        if (!$buku_exists) {
            $u = list_video_user_list::create([
                'id_user' => $request->id_user,
                'id_video' => $request->id_video,
            ]);
            if ($u === null) {
                return response()->json([
                    'status' => 204,
                    'message' => 'data User tidak di temukan',
                ], 201);
            } else {
                return response()->json([
                    'status' => 200,
                    'message' => 'Successfully',
                    'data' =>  $u,
                ], 200);
            }
        } else {
            return response()->json([
                'status' => 405,
                'message' => 'kamu sudah membeli video ini',
            ], 201);
        }
    }
    public function detail_video_books_free(Request $request)
    {
        $user = DB::table('list_video_user_free')
            ->join('video_free', 'list_video_user_free.id_video', '=', 'video_free.id')
            ->where('list_video_user_free.id_user', '=', $request->id_user)
            ->select('video_free.*', 'list_video_user_free.*')
            ->get();

        if ($user === null) {
            return response()->json([
                'status' => 200,
                'message' => 'data User tidak di temukan',
            ], 200);
        } else {
            return response()->json([
                'status' => 200,
                'message' => 'Successfully get data buku ',
                'id_user' => Datavideofree::collection(collect($user)),
            ], 200);
        }
    }
}
