<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VideoDetail extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'nama_video' => $this->nama_video,
            'description_video' => $this->description_video,
            'video_cover' => url('/') . $this->video_cover,
            'file_video' => url('/') . $this->file_video,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
