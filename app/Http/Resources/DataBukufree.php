<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DataBukufree extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'nama_buku' => $this->nama_buku,
            'description_buku' => $this->description_buku,
            'img_cover' => url('/') . $this->img_cover,
            'file_buku' => url('/') . $this->file_buku,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'id_user' => $this->id_user,
            'id_buku' => $this->id_buku,  
        ];
    }
}
