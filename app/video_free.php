<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class video_free extends Model
{
    protected $table = 'video_free';
    protected $fillable = [
        'nama_video', 'description_video', 'video_cover', 'file_video'
    ];



    public function list_video_user_free()
    {
        return $this->hasOne(list_video_user_free::class);
    }
}
