<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ebook_free extends Model
{
    protected $table = 'ebook_free';
    protected $fillable = [
        'nama_buku', 'description_buku', 'img_cover', 'file_buku'
    ];



    public function ebook_free()
    {
        return $this->hasOne(list_buku_user_free::class);
    }
}
