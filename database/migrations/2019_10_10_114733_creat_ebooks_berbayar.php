<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatEbooksBerbayar extends Migration
{
    public function up()
    {
        Schema::create('tbl_ebook', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->string('description')->unique();
            $table->string('img_path');
            $table->integer('price');
            $table->string('fdp_sample');
            $table->string('fdp_full');
            $table->bigInteger('id_gendre');
            $table->timestamps();
        });
    }
    public function down()
    { }
}
